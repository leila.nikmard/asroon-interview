import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import history from "./utils/helpers/history";
import Home from "./pages/Home";

const RouterComponent = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Home} />
      </Switch>
    </Router>
  );
};
export default RouterComponent;
