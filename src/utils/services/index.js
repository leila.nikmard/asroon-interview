import * as config from "../config";

export const getPostsService = () => fetch(`${config.API_BASE_URL}/posts` , {
    method: "GET"
})
