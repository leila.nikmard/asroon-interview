import React from 'react';
import RouterComponent from "./routes";
import "./App.css";

function App (){
    return(
      <main>
          <RouterComponent />
      </main>
    )
}

export default App;
