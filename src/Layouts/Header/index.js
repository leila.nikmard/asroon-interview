import React from "react";
import {images} from "../../utils/imageSource";
import "./style.css"

function Header (){
  const {logo} = images;
  
    return (
        <header>
          <div className="logo">
            <img src={logo}/>
          </div>
          <p className="call-number">02134000000</p>
        </header>
    );
}

export default Header;
