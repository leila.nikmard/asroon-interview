import React from "react";
import "./style.css"

function Footer (){
  return (
      <footer>
          <a
              href="https://www.asroon.ir/"
              target="_blank"
              rel="noopener noreferrer"
          >
              آسرون
          </a>
      </footer>
  );
}
export default Footer;
