import React  , {useState} from "react";
import Form from "../../Components/Form";
import DefaultLayout from "../../Layouts";
import Posts from "../../Components/Post";
import Table from "../../Components/Table";

function Home () {

    const [formData , setFormData] = useState({
        FirstName:"",
        LastName:"",
        NationalCode:"",
        Id:0
    });
    const [tableData , setTableData] = useState([]);
    const [isEdit , setIsEdit] = useState(false)
  
    const handleChange = (e) => {
      const {value ,name} = e.target;
      setFormData({
        ...formData ,
        [name] : value ,
        Id : isEdit ? formData.Id : tableData.length + 1
      })
    };
  
    const handleSubmit = (e) => {
      e.preventDefault();
      if(isEdit){
        const editTableData = tableData.map((it , index) => it.Id === formData.Id ? formData : it);
        setTableData(editTableData);
        setIsEdit(!isEdit);
  
      }else{
        setTableData([...tableData , formData]);
      }
      setFormData({
        FirstName:"",
        LastName:"",
        NationalCode:""
      })
    };
  
    const handleDelete = (id) => {
      const deleteTableData = tableData.filter((it , index) => it.Id !== id);
      setTableData(deleteTableData)
    }
  
  
    const handleEdit = (id) => {
      const editData = tableData.find((it , index) => it.Id === id);
      setIsEdit(!isEdit)
      setFormData(editData)
    }

    return(
        <DefaultLayout>
            <Form 
                data={formData} 
                isEdit={isEdit}
                handleChange={handleChange} 
                handleSubmit={handleSubmit}
            />
            <Table 
                data={tableData} 
                handleDelete={handleDelete} 
                handleEdit={handleEdit}
            />
            <Posts />
        </DefaultLayout>
    )
}

export default Home;
