import React from "react";
import "./style.css";

function Form({handleChange , data , handleSubmit , isEdit}){
    return (
        <div>
            <h2>ثبت نام</h2>
            <form>
                <label>
                    نام:
                    <input 
                        type="text" 
                        name="FirstName" 
                        onChange={handleChange}
                        value={data.FirstName}
                    />
                </label>
                <label>
                    نام خانوادگی:
                    <input 
                        type="text" 
                        name="LastName" 
                        onChange={handleChange} 
                        value={data.LastName}
                    />
                </label>
                <label>
                    کدملی:
                    <input 
                        type="text" 
                        name="NationalCode" 
                        onChange={handleChange} 
                        value={data.NationalCode}
                    />
                </label>
                <button className={`submit-btn ${isEdit ? "bgc-edit" : "bgc-submit"}`} type="submit" onClick={handleSubmit}>{isEdit ? "ویرایش" : "افزودن"}</button>
            </form>
        </div>
    )
}

export default Form;