import React, {useState } from "react";
import Loading from "../Loading";
import {getPostsService} from "../../utils/services/index";
import "./style.css";

function Posts(){
    const [PostsData , setPostsData] = useState([]);
    const [isLoading , setIsLoading] = useState(false);

    const handleGetPosts = () => {
        setIsLoading(true);
        getPostsService()
        .then(res => res.json())
        .then(response => {
            if(response){
                setPostsData([...response]);
                setIsLoading(false)
            }
        })
        .catch(error => console.log(error) );
    }

    return(
        <React.Fragment>
            <div>
                <button className="server-data-btn" onClick={handleGetPosts}>گرفتن پست ها از سرور</button>
            </div>
            {!isLoading && PostsData.length > 0 ? 
                <ul>
                    {PostsData.map((it , index) => (
                        <li>{it.title}</li>
                    ))}
                </ul>
                :
                <Loading isLoading={isLoading}/>
            }
        </React.Fragment>
    )
}

export default Posts;