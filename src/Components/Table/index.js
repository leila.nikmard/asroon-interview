import React from "react";
import NoItem from "../NoItem";
import "./style.css"

function Table ({data , handleDelete , handleEdit}){
    return(
        <table>
            <caption>لیست</caption>
            <thead>
                <tr>
                    <th>نام</th>
                    <th>نام خانوادگی</th>
                    <th>کد ملی</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                </tr>
            </thead>
            <tbody>
                {data.length > 0 ?
                    data.map((it , index) => (
                    <tr key={index}>
                        <td>{it.FirstName}</td>
                        <td>{it.LastName}</td>
                        <td>{it.NationalCode}</td>
                        <td>
                            <button className="edit-btn" onClick={() => handleEdit(it.Id)}>ویرایش</button>
                        </td>
                        <td>
                            <button className="delete-btn" onClick={() => handleDelete(it.Id)}>حذف</button>
                        </td>
                    </tr>
                    ))
                    :
                    <NoItem />
                }
            </tbody>
        </table>
    )
}

export default Table;