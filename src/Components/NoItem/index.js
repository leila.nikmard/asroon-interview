import React from "react";
import "./style.css";

function NoItem(){
    return (
        <tr className="no-item">
            <td colSpan="5">
                <span>اطلاعاتی یافت نشد.</span>
            </td>
        </tr>
    )
}

export default NoItem;